package tests.screens;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.testng.Assert;

public class PopularityScreen extends BaseScreen {

    public PopularityScreen(AppiumDriver<MobileElement> driver) throws Exception {
        super(driver);
    }

    //LOCATORS
    private By popularityBtn = By.id("ru.mamba.client:id/action_popularity");
    private By showcase = By.id("ru.mamba.client:id/promo_photoline");
    private By coinShowcaseTitle = By.className("android.widget.TextView");
    private By diamondBtn = By.id("ru.mamba.client:id/menu_item_diamond");
    private By alertTitle = By.id("ru.mamba.client:id/alertTitle");

    public void goToPopularity() {
        click(popularityBtn);
    }



    public void clickPhotoline() {
        waitUntiElementPresent(showcase);
        click(findElements(showcase).get(0));
    }

    public void clickLikes() {
        waitUntiElementPresent(showcase);
        click(findElements(showcase).get(1));
    }

    public void clickUp() {
        waitUntiElementPresent(showcase);
        click(findElements(showcase).get(2));
    }

    public void checkCoinShowcasePresence() {
        waitUntiElementPresent(diamondBtn);
        Assert.assertTrue(getText(coinShowcaseTitle).contains("Top up"));
    }

    public void checkIncognitoNotice() {
        Assert.assertEquals(getText(alertTitle), "Incognito");
    }

}
