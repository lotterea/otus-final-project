package tests.screens;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.testng.Assert;

public class SettingsScreen extends BaseScreen {

    public SettingsScreen(AppiumDriver<MobileElement> driver) throws Exception {
        super(driver);
    }

    //LOCATORS
    private By anketaBtn = By.id("ru.mamba.client:id/app_menu_profile");
    private By settingsBtn = By.id("ru.mamba.client:id/action_settings");
    private By privacyBtn = By.id("ru.mamba.client:id/category_privacy");
    private By incognitoSwitcher = By.id("ru.mamba.client:id/incognito_switch");
    private By vipSettings = By.id("ru.mamba.client:id/category_vip");
    private By mainSettings = By.id("ru.mamba.client:id/category_main");
    private By geolocationBtn = By.id("ru.mamba.client:id/geolocation_btn");
    private By popupTitle = By.id("ru.mamba.client:id/popup_title");
    private By mainBtn = By.id("ru.mamba.client:id/main_button");
    private By denyBtn = By.id("com.android.packageinstaller:id/permission_deny_button");
    private By getVipBtn = By.id("ru.mamba.client:id/get_vip_btn");
    private By text = By.className("android.widget.TextView");
    private By buyBtn = By.id("ru.mamba.client:id/buy_button");

    public void goToSettings() {
        click(anketaBtn);
        clickSettingsBtn();
    }

    public void clickSettingsBtn() {
        click(settingsBtn);
    }

    public void clickPrivacy() {
        click(privacyBtn);
    }

    public void turnOnIncognito() {
        waitUntiElementPresent(incognitoSwitcher);
        String status = getText(incognitoSwitcher);
        if (!status.equals("ON")) {
            click(incognitoSwitcher);
        }
    }

    public void turnOffIncognito() {
        waitUntiElementPresent(incognitoSwitcher);
        String status = getText(incognitoSwitcher);
        if (!status.equals("OFF")) {
            click(incognitoSwitcher);
        }
    }

    public void goToGeneral() {
        click(mainSettings);
    }

    public void clickGeolocationBtn() {
        click(geolocationBtn);
    }

    public void checkGeolocationNotice() {
        waitUntiElementPresent(popupTitle);
        Assert.assertTrue(getText(popupTitle).contains("Access to your coordinates"));
    }

    public void denyGeolocationRequest() {
        click(mainBtn);
        click(denyBtn);
        waitUntiElementPresent(geolocationBtn);
    }

    public void goToVipSettings() {
     click(vipSettings);
    }

    public void verifyVipShowcase() {
        click(getVipBtn);
        waitUntiElementPresent(buyBtn);
        Assert.assertTrue(getText(text).contains("VIP status is not active"));
    }


}
