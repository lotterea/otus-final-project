package tests.screens;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.testng.Assert;
import tests.helpers.ApiHelper;

import java.io.IOException;

public class VotingScreen extends BaseScreen {

    public VotingScreen(AppiumDriver<MobileElement> driver) throws Exception {
        super(driver);
    }

    //LOCATORS
    private By noBtn = By.id("ru.mamba.client:id/nope");
    private By cancelBtn = By.id("ru.mamba.client:id/left_button");
    private By notInterestedBtn = By.id("ru.mamba.client:id/right_button");
    private By name = By.id("ru.mamba.client:id/age");
    private By cardImage = By.id("ru.mamba.client:id/card_image");
    private By expandBtn = By.id("ru.mamba.client:id/expand_button");
    private By userInfo = By.id("ru.mamba.client:id/user_info");
    private By infoBlock = By.id("ru.mamba.client:id/info_block");
    private By avatar = By.id("ru.mamba.client:id/avatar_image");

    private void voteNo() {
        click(noBtn);
    }

    private void clickCancel() {
        click(cancelBtn);
    }

    private void clickConfirm() {
        click(notInterestedBtn);
    }

    public void checkCancellation() {
        String nameTxt = getText(name);
        voteNo();
        clickCancel();
        Assert.assertEquals(getText(name), nameTxt);
    }

    public void checkConfirmation() {
        String nameTxt = getText(name);
        voteNo();
        clickConfirm();
        Assert.assertNotEquals(getText(name), nameTxt);
    }

    public void voteNoBySwipe() {
        waitUntiElementPresent(cardImage);
        swipeElementLeft(cardImage);
    }

    public void checkAdditionalInfo() {
        while (findElements(expandBtn).size() == 0) {
            voteNoBySwipe();
            try {
                clickConfirm();
            } catch (Exception ignored) {
            }
        }
        click(expandBtn);
        waitUntiElementPresent(userInfo);
        click(expandBtn);
        waitUntiElementPresent(infoBlock);
    }

    public void checkStartScreen(String[] user) throws IOException {
        String startScreen = api.getStartScreen(user[0], user[1]);
        if (startScreen.equals("Voting")) {
            waitUntiElementPresent(cardImage);
        } else {
            waitUntiElementPresent(avatar);
        }
    }

}
