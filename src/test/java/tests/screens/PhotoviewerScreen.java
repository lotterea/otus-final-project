package tests.screens;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.testng.Assert;

public class PhotoviewerScreen extends BaseScreen {
    private AppiumDriver<MobileElement> driver;

    public PhotoviewerScreen(AppiumDriver<MobileElement> driver) throws Exception {
        super(driver);
        this.driver = driver;
    }

    //LOCATORS
    private By photo = By.id("ru.mamba.client:id/card_image");
    private By photoInAlbum = By.id("ru.mamba.client:id/photo");
    private By incognitoIcon = By.id("ru.mamba.client:id/incognito_icon");
    private By photoInPhotoviewer = By.id("ru.mamba.client:id/overlay_layout");
    private By noticeTitle = By.id("ru.mamba.client:id/notice_title");
    private By btn = By.className("android.widget.Button");
    private By incognitoSettings = By.id("ru.mamba.client:id/incognito_title");


    public void goToPhotoviewer() {
        click(photo);
        clickPhotoInAlbum();
    }

    public void checkIncognitoIsPresent() {
        waitUntiElementPresent(incognitoIcon);
    }

    public void clickPhotoInPhotoviewer() {
        click(photoInPhotoviewer);
    }

    public void clickPhotoInAlbum() {
        waitUntiElementPresent(photoInAlbum);
        findElements(photoInAlbum).get(1).click();
    }

    public void goToIncognitoSettingsFromNotice() {
        clickPhotoInPhotoviewer();
        Assert.assertTrue(getText(noticeTitle).contains("Photo is hidden"));
        findElements(btn).get(1).click();
        waitUntiElementPresent(incognitoSettings);
    }

    public void verifyAbsenceOfIncognito() {
        waitUntiElementPresent(photoInPhotoviewer);
        waitUntiElementInvisible(incognitoIcon);
    }


}
