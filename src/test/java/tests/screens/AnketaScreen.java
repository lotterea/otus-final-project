package tests.screens;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;

public class AnketaScreen extends BaseScreen {

    public AnketaScreen(AppiumDriver<MobileElement> driver) throws Exception {
        super(driver);
    }

    //LOCATORS
    private By anketaBtn = By.id("ru.mamba.client:id/app_menu_profile");
    private By verificationIcon = By.id("ru.mamba.client:id/verification_icon");
    private By editBtn = By.id("ru.mamba.client:id/edit_button");
    private By fieldName = By.id("ru.mamba.client:id/field_name");
    private By aboutMeTextarea = By.id("ru.mamba.client:id/edit_text");
    private By btn = By.className("android.widget.Button");
    private By aboutMe = By.id("ru.mamba.client:id/section_text");
    private By indicateTags = By.id("ru.mamba.client:id/section_button");
    private By nonExistantTag = By.id("none");
    private By findTag = By.id("ru.mamba.client:id/find_interest");
    private By alertTitle = By.id("ru.mamba.client:id/alertTitle");
    private By laterBtn = By.id("android:id/button1");
    private By balanceString = By.id("ru.mamba.client:id/add_coins_text");



    public void goToAnketa() {
        click(anketaBtn);
        waitUntiElementPresent(verificationIcon);
    }

    public void clickChangeAboutMe() {
        click(editBtn);
        waitUntiElementPresent(fieldName);
        findElements(fieldName).get(2).click();
        waitUntiElementPresent(aboutMeTextarea);
    }

    public void changeAboutMeAndCheck() {
        String text = getText(aboutMeTextarea);
        if (!text.equals("I'm serious")) {
            text = "I'm serious";
        } else {
            text = "I'm funny";

        }
        find(aboutMeTextarea).clear();
        find(aboutMeTextarea).sendKeys(text);
        find(btn).click();
        waitUntiElementPresent(fieldName);
        goBack();
        waitUntiElementPresent(verificationIcon);
        scrollDownToElement(aboutMe);
        Assert.assertTrue(getText(aboutMe).contains(text));
    }


    public void changeTags() {
       scrollDownToElement(indicateTags);
       click(indicateTags);
       waitUntiElementPresent(findTag);
       click(nonExistantTag);
    }

    public void goToCascade() {
        click(editBtn);
        waitUntiElementPresent(fieldName);
    }

    public void cancelFillingCascade() {
        findElements(fieldName).get(5).click();
        goBack();
        Assert.assertTrue(getText(alertTitle).contains("Cancel filling"));
        click(laterBtn);
        waitUntiElementPresent(fieldName);
    }

    public void checkBalance(String[] user) throws Exception {
        String serverBalance = api.getBalance(user[0], user[1]);
        String actualBalance = "";
        goToAnketa();
        actualBalance = parse.parseDigits(getText(balanceString));
        Assert.assertEquals(serverBalance, actualBalance);
    }
}
