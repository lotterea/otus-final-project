package tests.screens;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import tests.helpers.*;

import java.time.Duration;
import java.util.List;

public class BaseScreen {

    private AppiumDriver<MobileElement> driver;
    private AnketaHelper anketaHelper = new AnketaHelper();
    ApiHelper api = new ApiHelper();
    JsonHelper json = new JsonHelper();
    ParsingHelper parse = new ParsingHelper();


    public BaseScreen(AppiumDriver<MobileElement> driver) throws Exception {
        this.driver = driver;
    }

    private WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), 5);

    //LOCATORS
    private By signInWithEmailBtn = By.id("ru.mamba.client:id/default_auth_btn");
    private By googleCancelBtn = By.id("com.google.android.gms:id/cancel");
    private By loginField = By.id("ru.mamba.client:id/fb_widget_main_textview");
    private By passwordField = By.id("ru.mamba.client:id/fb_widget_main_textview");
    private By signInBtn = By.id("ru.mamba.client:id/btn_signin");
    private By autofillNo = By.id("android:id/autofill_save_no");
    private By popularity = By.id("ru.mamba.client:id/action_popularity");
    private By mainScreenBtn = By.id("ru.mamba.client:id/app_menu_home");

    public void goToMainScreen() {
        click(mainScreenBtn);
        waitUntiElementPresent(popularity);
    }


    public void authorize(String email, String password) {
        waitUntiElementPresent(signInWithEmailBtn);
        click(signInWithEmailBtn);
        try {
            click(googleCancelBtn);
        } catch (Exception ignored) {
        }
        waitUntiElementPresent(loginField);
        findElements(loginField).get(0).sendKeys(email);
        findElements(passwordField).get(1).sendKeys(password);
        click(signInBtn);
        try {
            click(autofillNo);
        } catch (Exception ignored) {
        }
        waitUntiElementPresent(popularity);
    }

    public void authorizeByTag(String tag) throws Exception {
        String[] anketa = anketaHelper.getAnketaByTag(tag);
        authorize(anketa[0], anketa[1]);
    }

    public String[] getUserByTag(String tag) throws Exception {
        String[] anketa = anketaHelper.getAnketaByTag(tag);
        String[] user = new String[] {anketa[0], anketa[1]};
        return user;
    }

    public void click(By by) {
        waitUntiElementPresent(by);
        find(by).click();
    }

    public void click(WebElement element) {
        element.click();
    }

    void waitUntiElementPresent(By locator) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }


    void waitUntiElementInvisible(By locator) {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }


    private void scroll() {
        TouchAction action = new TouchAction(driver);
        Dimension size = driver.manage().window().getSize();
        int x = size.width / 2;
        int start_y = (int) (size.height * 0.1);
        int finish_y = (int) (size.height * 0.8);
        action
                .press(PointOption.point(x, finish_y))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(10)))
                .moveTo(PointOption.point(x, start_y))
                .release()
                .perform();
    }


    void scrollDownToElement(By by) {
        while (driver.findElements(by).size() == 0) {
            scroll();
        }
    }

    void swipeElementLeft(By by) {
        WebElement element = driver.findElement(by);
        int left_x = element.getLocation().getX();
        int upper_y = element.getLocation().getY();
        int lower_y = upper_y + element.getSize().getHeight();
        int middle_y = (upper_y + lower_y) / 2;

        int right_x = left_x + element.getSize().getWidth();


        TouchAction action = new TouchAction(driver);
        action
                .press(PointOption.point(right_x, middle_y))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(10)))
                .moveTo(PointOption.point(left_x, middle_y))
                .release()
                .perform();
    }

    public void goBack() {
        driver.navigate().back();
    }

    MobileElement find(By by) {
        return driver.findElement(by);
    }

    List<MobileElement> findElements(By by) {
        return driver.findElements(by);
    }

    String getText(By by) {
        waitUntiElementPresent(by);
        return find(by).getText();
    }
}
