package tests.tests;

import io.qameta.allure.Description;
import org.testng.annotations.Test;

public class PopularityTests extends Base {

    @Test
    @Description(value = "Открытие витрины фотолинейки пользователем без монет")
    public void checkPhotolineShowcaseNoMoney() throws Exception {
        base.authorizeByTag("popularityTests");
        popularity.goToPopularity();
        popularity.clickPhotoline();
        popularity.checkCoinShowcasePresence();

    }

    @Test
    @Description(value = "Открытие витрины поднятия наверх пользователем без монет")
    public void checkUpShowcaseNoMoney() throws Exception {
        base.authorizeByTag("popularityTests");
        popularity.goToPopularity();
        popularity.clickUp();
        popularity.checkCoinShowcasePresence();
    }

    @Test
    @Description(value = "Открытие витрины показов пользователем-инкогнито")
    public void checkLikesShowcaseIncognito() throws Exception {
        base.authorizeByTag("popularityTests");
        settings.goToSettings();
        settings.clickPrivacy();
        settings.turnOnIncognito();
        settings.goBack();
        settings.goBack();
        base.goToMainScreen();
        popularity.goToPopularity();
        popularity.clickLikes();
        popularity.checkIncognitoNotice();
    }
}