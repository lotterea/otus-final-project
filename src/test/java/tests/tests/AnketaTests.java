package tests.tests;

import io.qameta.allure.Description;
import org.testng.annotations.Test;

public class AnketaTests extends Base {

    @Test
    @Description(value = "Редактирование поля \"Обо мне\"")
    public void changeAboutMe() throws Exception {
        base.authorizeByTag("anketaTests");
        anketa.goToAnketa();
        anketa.clickChangeAboutMe();
        anketa.changeAboutMeAndCheck();
    }

    @Test
    @Description(value = "Добавление тегов - тест намеренно завален")
    public void checkEditTags() throws Exception {
        base.authorizeByTag("anketaTests");
        anketa.goToAnketa();
        anketa.changeTags();
    }

    @Test
    @Description(value = "Отмена заполнения каскада")
    public void checkCancelCascade() throws Exception {
        base.authorizeByTag("anketaTests");
        anketa.goToAnketa();
        anketa.goToCascade();
        anketa.cancelFillingCascade();
    }

    @Test
    @Description(value = "Проверяем соответствие отображаемого баланса данным с сервера")
    public void checkBalance() throws Exception {
        base.authorizeByTag("moneyTest");
        anketa.checkBalance(base.getUserByTag("moneyTest"));
    }
}