package tests.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import tests.helpers.AnketaHelper;
import tests.helpers.ApiHelper;
import tests.helpers.DriverManager;
import tests.listeners.TestListener;
import tests.screens.*;

@Listeners(TestListener.class)
public class Base {

    BaseScreen base;
    VotingScreen voting;
    PopularityScreen popularity;
    SettingsScreen settings;
    PhotoviewerScreen photoviewer;
    AnketaScreen anketa;

    @BeforeMethod(alwaysRun = true)
    public void setUp() throws Exception {
        DriverManager.setDriver();
        base = new BaseScreen(DriverManager.getDriver());
        voting = new VotingScreen(DriverManager.getDriver());
        popularity = new PopularityScreen(DriverManager.getDriver());
        settings = new SettingsScreen(DriverManager.getDriver());
        photoviewer = new PhotoviewerScreen(DriverManager.getDriver());
        anketa = new AnketaScreen(DriverManager.getDriver());
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown() {
        if (DriverManager.getDriver() != null) DriverManager.getDriver().quit();
    }
}
