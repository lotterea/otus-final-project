package tests.tests;

import io.qameta.allure.Description;
import org.testng.annotations.Test;


public class VotingTests extends Base {

    @Test
    @Description(value = "Отмена голоса")
    public void checkCancelVote() throws Exception {
        base.authorizeByTag("votingTests");
        voting.checkCancellation();
    }

    @Test
    @Description(value = "Подтверждение голоса")
    public void checkConfirmVote() throws Exception {
        base.authorizeByTag("votingTests");
        voting.checkConfirmation();
    }

    @Test
    @Description(value = "Голосование свайпом")
    public void checkVoteBySwipe() throws Exception {
        base.authorizeByTag("votingTests");
        voting.voteNoBySwipe();
    }

    @Test
    @Description(value = "Просмотр дополнительной информации в карточке")
    public void checkAdditionalInfo() throws Exception {
        base.authorizeByTag("votingTests");
        voting.checkAdditionalInfo();
    }

    @Test
    @Description(value = "Соответствие стартового экрана настройке, присылаемой с сервера")
    public void startScreen() throws Exception {
        base.authorizeByTag("votingTests");
        voting.checkStartScreen(base.getUserByTag("votingTests"));
    }
}