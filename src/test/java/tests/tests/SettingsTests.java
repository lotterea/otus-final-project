package tests.tests;

import io.qameta.allure.Description;
import org.testng.annotations.Test;

public class SettingsTests extends Base {

    @Test
    @Description(value = "Включение и выключение инкогнито")
    public void turnOnTurnOffIncognito() throws Exception {
        base.authorizeByTag("settingsTests");
        settings.goToSettings();
        settings.clickPrivacy();
        settings.turnOnIncognito();
        settings.goBack();
        settings.goBack();
        photoviewer.goToPhotoviewer();
        photoviewer.checkIncognitoIsPresent();
        photoviewer.goToIncognitoSettingsFromNotice();
        settings.turnOffIncognito();
        settings.goBack();
        settings.goBack();
        photoviewer.clickPhotoInAlbum();
        photoviewer.verifyAbsenceOfIncognito();
    }

    @Test
    @Description(value = "Отказ в разрешении на доступ к геолокации")
    public void checkGeolocationRequest() throws Exception {
        base.authorizeByTag("settingsTests");
        settings.goToSettings();
        settings.goToGeneral();
        settings.clickGeolocationBtn();
        settings.checkGeolocationNotice();
        settings.denyGeolocationRequest();
    }

    @Test
    @Description(value = "Открытие VIP-настроек пользователем без VIP")
    public void checkVipSettingsWithoutVip() throws Exception {
        base.authorizeByTag("settingsTests");
        settings.goToSettings();
        settings.goToVipSettings();
        settings.verifyVipShowcase();
    }


}