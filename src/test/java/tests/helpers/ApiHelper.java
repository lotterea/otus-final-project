package tests.helpers;

import org.apache.http.Header;
import org.apache.http.HttpResponse;

import java.io.*;
import java.util.Properties;

public class ApiHelper extends HttpHelper {

    public ApiHelper() throws Exception {
    }

    private JsonHelper json = new JsonHelper();
    private PropertyManager pm = new PropertyManager();

    private String baseUrl = pm.getProperties("baseUrl");
    private String mobileApiv5Url = pm.getProperties("mobileApiv5Url");


    private String authorizeOnServer(String login, String password) throws IOException {
        String url = mobileApiv5Url + "/login/builder/?langId=ru&dateType=timestamp";
        String data = "{\"login\":{\"login\":\"" + login + "\",\"password\":\"" + password + "\"}}";

        HttpResponse resPost = post(url, data, null);
        Header[] setCookie = resPost.getHeaders("Set-Cookie");

        StringBuilder cookie = new StringBuilder();
        for (Header header : setCookie) {
            cookie.append(header.getValue());
            cookie.append("; ");
        }
        return cookie.toString();
    }

    public String getStartScreen(String login, String password) throws IOException {
        return json.getJson(getResponse(get(baseUrl + "/start_screen", authorizeOnServer(login, password)))).getString("startScreen");
    }

    public String getBalance(String login, String password) throws IOException {
        double balance;
        String money;
        balance = json.getJson(getResponse(get(baseUrl + "/profiles/my?withDetails=true", authorizeOnServer(login, password)))).getDouble("balance");
        if (balance % 1 == 0) {
            money = String.valueOf((int) balance);
        } else {
            money = String.valueOf(balance);
        }
        return money;
    }
}

