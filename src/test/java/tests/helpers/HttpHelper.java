package tests.helpers;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


class HttpHelper extends JsonHelper{

    HttpResponse get(String url, String cookie) throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        request.addHeader("Cookie", cookie);
        return client.execute(request);
    }

    HttpResponse post(String url, String data, ArrayList<String> headers) throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost requestPost = new HttpPost(url);
        JSONObject jsonObj = getJson(data);
        StringEntity entity = new StringEntity(jsonObj.toString(), "UTF-8");
        entity.setContentType("application/json");
        requestPost.setEntity(entity);

        if (headers != null) {
            for (String header : headers) {
                String[] split = header.split(":");
                requestPost.addHeader(split[0].trim(), split[1].trim());
            }
        }

        return client.execute(requestPost);
    }

    String getResponse(HttpResponse response) {
        InputStream is;
        BufferedReader br;
        StringBuilder sb = new StringBuilder();
        try {
            is = response.getEntity().getContent();
            String line;
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }


}

