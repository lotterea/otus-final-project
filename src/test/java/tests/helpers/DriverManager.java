package tests.helpers;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;


public class DriverManager {
    static AppiumDriver<MobileElement> driver;
    static String home = new File(System.getProperty("user.dir")).toString();
    static Path path = java.nio.file.Paths.get(home, "src", "test", "resources", "Apps", "mamba.apk");

    public static void setDriver() throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("avd","Nexus_5_API_28");
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9.0");
        caps.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
        caps.setCapability("app", path.toString());
        caps.setCapability("appPackage", "ru.mamba.client");
        caps.setCapability("appWaitActivity", ".v2.view.onboarding.OnboardingActivity");
        URL appiumUrl = new URL("http://127.0.0.1:4723/wd/hub");
        driver = new AppiumDriver<MobileElement>(appiumUrl, caps);
    }

    public static AppiumDriver<MobileElement> getDriver() {
        return driver;
    }

}
