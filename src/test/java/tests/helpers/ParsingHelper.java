package tests.helpers;

public class ParsingHelper {

    public String parseDigits(String string) {
        double digits = Double.parseDouble(string.replaceAll("[^0-9?!\\.]",""));
        if (digits % 1 == 0) {
            return String.valueOf((int) digits);
        } else {
            return String.valueOf(digits);
        }
    }
}
