package tests.helpers;

import org.json.JSONObject;

public class JsonHelper {
    public JSONObject getJson(String json) {
        return new JSONObject(json);
    }
}
