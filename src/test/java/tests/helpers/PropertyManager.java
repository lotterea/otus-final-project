package tests.helpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Properties;

public class PropertyManager {
    static String home = new File(System.getProperty("user.dir")).toString();
    static Path path = java.nio.file.Paths.get(home, "src", "test", "resources", "test.properties");

    String getProperties(String property) throws IOException {

        InputStream input = new FileInputStream(path.toString());
        Properties properties = new Properties();
        properties.load(input);
        return properties.getProperty(property);
    }

}
