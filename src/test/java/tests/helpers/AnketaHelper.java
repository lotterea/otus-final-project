package tests.helpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

public class AnketaHelper {
    private static String home = new File(System.getProperty("user.dir")).toString();
    private static Path path = java.nio.file.Paths.get(home, "src", "test", "resources", "logins.csv");

    public String[] getAnketaByTag(String tag) throws Exception {

        String csvFile = path.toString();
        String line = "";
        String cvsSplitBy = ",";

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {

                String[] data = line.split(cvsSplitBy);

                if (data[2].equals(tag)) {
                    return data;
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new Exception("User not found");
    }
}

