package tests.listeners;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.testng.ITestListener;
import org.testng.ITestResult;
import tests.helpers.DriverManager;

import java.io.File;

public class TestListener implements ITestListener {

    private String screenshotName = "screenshot";
    private String home = new File(System.getProperty("user.dir")).toString();
    private String path = "";

    @Override
    public void onTestFailure(ITestResult result) {
        System.out.println("***** Error " + result.getName() + " test has failed *****");
        screenshotName = result.getName() +  System.currentTimeMillis() + ".png";
        path = java.nio.file.Paths.get(home, "src", "test", "Screenshots", screenshotName).toString();
        try {
            takeScreenshot(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void takeScreenshot(String path) throws Exception {
        File file = DriverManager.getDriver().getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File(path));
    }
}